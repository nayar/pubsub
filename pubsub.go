package pubsub

import (
	"container/heap"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

const (
	PolDrop   = 0
	PolRotate = 1
	PolBlock  = 2
)

type Pubsub struct {
	lock          sync.RWMutex
	subscriptions map[string]map[*subscriber]struct{}
	subscribers   map[*subscriber]map[string]struct{}
}

type subscriber struct {
	lock       sync.Mutex
	ps         *Pubsub
	wakeup     chan struct{}
	spaceAvail chan struct{}
	queue      *PriorityQueue
	queueSize  int
	overflow   int
	policy     int
}

type Subscriber struct {
	*subscriber
}

type Message struct {
	From string
	To   string
	Val  interface{}
	Pri  int
	ord  int64
}

type PriorityQueue []*Message

var defaultInstance *Pubsub
var moduleLock sync.Mutex
var uidCounter int64 = 0
var autoCollect = true

func AutoCollect() bool {
	return autoCollect
}

func SetAutoCollect(v bool) {
	autoCollect = v
}

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	if pq[i].Pri == pq[j].Pri {
		return pq[i].ord < pq[j].ord
	}
	return pq[i].Pri > pq[j].Pri
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PriorityQueue) Push(x interface{}) {
	*pq = append(*pq, x.(*Message))
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	*pq = old[0 : n-1]
	return item
}

func GetUid() int64 {
	return atomic.AddInt64(&uidCounter, 1)
}

func GetUidStr() string {
	return strconv.FormatInt(GetUid(), 10)
}

func DefaultInstance() *Pubsub {
	moduleLock.Lock()
	defer moduleLock.Unlock()
	if defaultInstance == nil {
		defaultInstance = New()
	}
	return defaultInstance
}

func subscriberFinalizer(s *Subscriber) {
	s.Clear()
}

func New() *Pubsub {
	ps := new(Pubsub)
	ps.subscriptions = make(map[string]map[*subscriber]struct{})
	ps.subscribers = make(map[*subscriber]map[string]struct{})
	return ps
}

func (ps *Pubsub) NewSubscriber() *Subscriber {
	s := new(subscriber)
	s.ps = ps
	s.wakeup = make(chan struct{}, 1)
	s.spaceAvail = make(chan struct{}, 1)
	s.queue = &PriorityQueue{}
	su := &Subscriber{subscriber: s}
	if autoCollect {
		runtime.SetFinalizer(su, subscriberFinalizer)
	}
	return su
}

func (s *Subscriber) SetQueueSize(size int) *Subscriber {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.queueSize = size
	return s
}

func (s *Subscriber) QueueSize() int {
	s.lock.Lock()
	defer s.lock.Unlock()
	return s.queueSize
}

func (s *Subscriber) SetPolicy(pol int) *Subscriber {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.policy = pol
	return s
}

func (s *Subscriber) Policy() int {
	s.lock.Lock()
	defer s.lock.Unlock()
	return s.policy
}

func (s *Subscriber) Overflow() int {
	return s.resetOverflow()
}

func (s *Subscriber) Wait(t time.Duration) (msg *Message, ok bool) {
	var timeoutCh <-chan time.Time
	if t > 0 {
		timeoutCh = time.After(t)
	}
	for {
		if msg, ok = s.pull(); ok {
			return
		}
		if t < 0 {
			return
		}
		select {
		case <-s.wakeup:
		case <-timeoutCh:
			return
		}
	}
	return
}

func (s *Subscriber) WakeupChan() <-chan struct{} {
	return s.wakeup
}

func (s *Subscriber) Waiting() int {
	return s.waiting()
}

func (s *Subscriber) Flush() *Subscriber {
	s.flush()
	return s
}

func (s *Subscriber) Clear() *Subscriber {
	s.unsubscribeAll(true)
	s.flush()
	return s
}

func (s *Subscriber) Subscribe(keys ...string) *Subscriber {
	s.ps.lock.Lock()
	defer s.ps.lock.Unlock()
	for _, key := range keys {
		s.subscribe(key, false)
	}
	return s
}

func (s *Subscriber) Unsubscribe(keys ...string) *Subscriber {
	s.ps.lock.Lock()
	defer s.ps.lock.Unlock()
	for _, key := range keys {
		s.unsubscribe(key, false)
	}
	return s
}

func (s *Subscriber) UnsubscribeAll() *Subscriber {
	s.unsubscribeAll(true)
	return s
}

func (s *Subscriber) Nsubscriptions() int {
	return s.nsubscriptions(true)
}

func (ps *Pubsub) Send(msg *Message) {
	ps.lock.RLock()
	defer ps.lock.RUnlock()
	msg.ord = GetUid()
	chunks := strings.Split(msg.To, ".")
	var key string
	for _, chunk := range chunks {
		key += chunk
		if subscribers, exists := ps.subscriptions[key]; exists {
			for s := range subscribers {
				if s.policy == PolBlock {
					ps.lock.RUnlock()
					s.push(msg)
					ps.lock.RLock()
				} else {
					s.push(msg)
				}
			}
		}
		key += "."
	}
}

func (ps *Pubsub) Nsubscribers(key string) int {
	ps.lock.RLock()
	defer ps.lock.RUnlock()
	return len(ps.subscriptions[key])
}

func (s *subscriber) nsubscriptions(lock bool) int {
	if lock {
		s.ps.lock.RLock()
		defer s.ps.lock.RUnlock()
	}
	return len(s.ps.subscribers[s])
}

func (s *subscriber) subscribe(key string, lock bool) {
	if lock {
		s.ps.lock.Lock()
		defer s.ps.lock.Unlock()
	}

	if _, exists := s.ps.subscriptions[key]; !exists {
		s.ps.subscriptions[key] = make(map[*subscriber]struct{})
	}
	if _, exists := s.ps.subscribers[s]; !exists {
		s.ps.subscribers[s] = make(map[string]struct{})
	}
	s.ps.subscriptions[key][s] = struct{}{}
	s.ps.subscribers[s][key] = struct{}{}
}

func (s *subscriber) unsubscribe(key string, lock bool) {
	if lock {
		s.ps.lock.Lock()
		defer s.ps.lock.Unlock()
	}

	if _, exists := s.ps.subscriptions[key]; exists {
		delete(s.ps.subscriptions[key], s)
		if len(s.ps.subscriptions[key]) == 0 {
			delete(s.ps.subscriptions, key)
		}
	}
	if _, exists := s.ps.subscribers[s]; exists {
		delete(s.ps.subscribers[s], key)
		if len(s.ps.subscribers[s]) == 0 {
			delete(s.ps.subscribers, s)
		}

	}

}

func (s *subscriber) unsubscribeAll(lock bool) {
	if lock {
		s.ps.lock.Lock()
		defer s.ps.lock.Unlock()
	}

	for key := range s.ps.subscribers[s] {
		s.unsubscribe(key, false)
	}
}

func (s *subscriber) resetOverflow() (ret int) {
	s.lock.Lock()
	defer s.lock.Unlock()
	ret = s.overflow
	s.overflow = 0
	return
}

func (s *subscriber) push(msg *Message) {
	s.lock.Lock()
	if s.queueSize > 0 && len(*s.queue) >= s.queueSize {
		switch s.policy {
		case PolDrop:
			s.overflow += 1
		case PolRotate:
			heap.Pop(s.queue)
			heap.Push(s.queue, msg)
			s.overflow += 1
		case PolBlock:
			for {
				s.lock.Unlock()
				<-s.spaceAvail
				s.lock.Lock()
				if len(*s.queue) < s.queueSize {
					heap.Push(s.queue, msg)
					if len(*s.queue) < s.queueSize {
						select {
						case s.spaceAvail <- struct{}{}:
						default:
						}
					}
					break
				}

			}

		}

	} else {
		heap.Push(s.queue, msg)
	}
	select {
	case s.wakeup <- struct{}{}:
	default:
	}
	s.lock.Unlock()
}

func (s *subscriber) pull() (*Message, bool) {
	s.lock.Lock()
	defer s.lock.Unlock()
	if len(*s.queue) == 0 {
		return nil, false
	}
	e := heap.Pop(s.queue)
	if len(*s.queue) > 0 {
		select {
		case s.wakeup <- struct{}{}:
		default:
		}
	}
	if s.queueSize > 0 && s.policy == PolBlock && len(*s.queue) < s.queueSize {
		select {
		case s.spaceAvail <- struct{}{}:
		default:
		}
	}

	return e.(*Message), true
}

func (s *subscriber) waiting() int {
	s.lock.Lock()
	defer s.lock.Unlock()
	return len(*s.queue)
}

func (s *subscriber) flush() {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.queue = &PriorityQueue{}
}

func (ps *Pubsub) Find(pattern string) []string {
	ps.lock.RLock()
	defer ps.lock.RUnlock()

	res := make([]string, 0)
	r, err := regexp.Compile(pattern)
	if err == nil {
		for key := range ps.subscriptions {
			if r.MatchString(key) {
				res = append(res, key)
			}
		}
	}
	return res
}
