package pubsub

import (
	"runtime"
	"testing"
	"time"
)

func TestPubsubInit(t *testing.T) {
	ps := DefaultInstance()
	if ps == nil {
		t.Fail()
	}
}

func TestRegisterUnregister(t *testing.T) {
	ps := DefaultInstance()
	s := ps.NewSubscriber()
	s.Subscribe("root")
	if ps.Nsubscribers("root") != 1 {
		t.Errorf("num subscribers missmatch")
	}
	ps.Send(&Message{To: "root", Val: 1})
	ps.Send(&Message{To: "root", Val: 2})
	if s.Waiting() != 2 {
		t.Errorf("num messages missmatch")
	}
	if res, ok := s.Wait(time.Millisecond); ok {
		t.Logf("Message came in, it is ok: %#v\r\n", res)
	} else {
		t.Errorf("Message not came in")
	}
	if res, ok := s.Wait(time.Millisecond); ok {
		t.Logf("Message came in, it is ok: %#v\r\n", res)
	} else {
		t.Errorf("Message not came in")
	}
	if s.Waiting() != 0 {
		t.Errorf("num messages missmatch")
	}
	s.Unsubscribe("root")
	if ps.Nsubscribers("root") != 0 {
		t.Errorf("num subscribers missmatch")
	}
	if s.Nsubscriptions() != 0 {
		t.Errorf("num subscriptions missmatch")
	}
	ps.Send(&Message{To: "root", Val: 7})
	if _, ok := s.Wait(time.Millisecond); ok {
		t.Errorf("Recibed message when unsubscribed")
	} else {
		t.Logf("Message not came in, it is ok\r\n")
	}
	s.Subscribe("sota", "caballo", "rey")
	if s.Nsubscriptions() != 3 {
		t.Error("num subscriptions missmatch, must be 3")
	}
	s.Unsubscribe("caballo", "rey")
	if s.Nsubscriptions() != 1 {
		t.Error("num subscriptions missmatch, must be 1")
	}
	s.UnsubscribeAll()
}

func TestOrder(t *testing.T) {
	ps := DefaultInstance()
	s := ps.NewSubscriber()
	s.Subscribe("root")
	ps.Send(&Message{To: "root", Val: "sota"})
	ps.Send(&Message{To: "root", Val: "caballo"})
	ps.Send(&Message{To: "root", Val: "rey"})
	if s.Waiting() != 3 {
		t.Errorf("num messages missmatch")
	}
	res, _ := s.Wait(time.Millisecond)
	if res.Val != "sota" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	res, _ = s.Wait(time.Millisecond)
	if res.Val != "caballo" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	res, _ = s.Wait(time.Millisecond)
	if res.Val != "rey" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	s.Clear()

}

func TestPriorityOrder(t *testing.T) {
	ps := DefaultInstance()
	s := ps.NewSubscriber()
	s.Subscribe("root")
	ps.Send(&Message{To: "root", Val: "sota"})
	ps.Send(&Message{To: "root", Val: "caballo", Pri: 1}) //Default priority is 0
	ps.Send(&Message{To: "root", Val: "rey"})
	ps.Send(&Message{To: "root", Val: "as", Pri: 2})
	if s.Waiting() != 4 {
		t.Errorf("num messages missmatch")
	}
	res, _ := s.Wait(time.Millisecond)
	if res.Val != "as" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	res, _ = s.Wait(time.Millisecond)
	if res.Val != "caballo" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	res, _ = s.Wait(time.Millisecond)
	if res.Val != "sota" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	res, _ = s.Wait(time.Millisecond)
	if res.Val != "rey" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	s.Clear()
}

func TestFlush(t *testing.T) {
	ps := DefaultInstance()
	s := ps.NewSubscriber()
	s.Subscribe("root")
	ps.Send(&Message{To: "root", Val: 1})
	ps.Send(&Message{To: "root", Val: 2})
	s.Flush()
	if s.Waiting() != 0 {
		t.Errorf("num messages missmatch")
	}
	if s.Nsubscriptions() != 1 {
		t.Errorf("num subscriptions missmatch")
	}
	s.UnsubscribeAll()
}

func TestClear(t *testing.T) {
	ps := DefaultInstance()
	s := ps.NewSubscriber()
	s.Subscribe("root")
	ps.Send(&Message{To: "root", Val: 1})
	ps.Send(&Message{To: "root", Val: 2})
	s.Clear()
	if s.Waiting() != 0 {
		t.Errorf("num messages missmatch")
	}
	if s.Nsubscriptions() != 0 {
		t.Errorf("num subscriptions missmatch")
	}
}

func TestPathSubscription(t *testing.T) {
	ps := DefaultInstance()
	s1 := ps.NewSubscriber()
	s1.Subscribe("root")
	s2 := ps.NewSubscriber()
	s2.Subscribe("root.card")
	s3 := ps.NewSubscriber()
	s3.Subscribe("root.card.sota")
	ps.Send(&Message{To: "root.card.sota", Val: 1})
	ps.Send(&Message{To: "root.card", Val: 2})
	ps.Send(&Message{To: "root", Val: 3})
	if s1.Waiting() != 3 {
		t.Errorf("num messages missmatch")
	}
	if s2.Waiting() != 2 {
		t.Errorf("num messages missmatch")
	}
	if s3.Waiting() != 1 {
		t.Errorf("num messages missmatch")
	}
	s1.Clear()
	s2.Clear()
	s3.Clear()
}

func subscribe() {
	ps := DefaultInstance()
	s := ps.NewSubscriber()
	s.Subscribe("testGC")
	// s is garbage at function exit
}

func TestAutoCollectEnable(t *testing.T) {
	SetAutoCollect(true)
	subscribe()
	ps := DefaultInstance()
	if ps.Nsubscribers("testGC") == 0 {
		t.Logf("GC too fast, subscriber already collected")
	}
	runtime.GC()
	time.Sleep(10 * time.Millisecond)
	runtime.GC()
	if ps.Nsubscribers("testGC") != 0 {
		t.Errorf("Subscription to testGC not collected")
	}
}

func TestAutoCollectDisable(t *testing.T) {
	SetAutoCollect(false)
	subscribe()
	ps := DefaultInstance()
	if ps.Nsubscribers("testGC") == 0 {
		t.Errorf("Object collected")
	}
	runtime.GC()
	time.Sleep(10 * time.Millisecond)
	runtime.GC()
	if ps.Nsubscribers("testGC") == 0 {
		t.Errorf("Object collected")
	}
}

func TestRotatePolicy(t *testing.T) {
	ps := DefaultInstance()
	s := ps.NewSubscriber().SetQueueSize(2).SetPolicy(PolRotate)
	s.Subscribe("root")
	ps.Send(&Message{To: "root", Val: "sota"})
	ps.Send(&Message{To: "root", Val: "caballo"})
	ps.Send(&Message{To: "root", Val: "rey"})
	if s.Waiting() != 2 {
		t.Errorf("num messages missmatch")
	}

	if s.Overflow() != 1 {
		t.Errorf("No overflow")
	}

	if s.Overflow() != 0 {
		t.Errorf("Overflow counter was not reset")
	}

	res, _ := s.Wait(time.Millisecond)
	if res.Val != "caballo" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	res, _ = s.Wait(time.Millisecond)
	if res.Val != "rey" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	s.Clear()
}

func TestDropPolicy(t *testing.T) {
	ps := DefaultInstance()
	s := ps.NewSubscriber().SetQueueSize(2).SetPolicy(PolDrop)
	s.Subscribe("root")
	ps.Send(&Message{To: "root", Val: "sota"})
	ps.Send(&Message{To: "root", Val: "caballo"})
	ps.Send(&Message{To: "root", Val: "rey"})
	if s.Waiting() != 2 {
		t.Errorf("num messages missmatch")
	}

	if s.Overflow() != 1 {
		t.Errorf("No overflow")
	}

	if s.Overflow() != 0 {
		t.Errorf("Overflow counter was not reset")
	}

	res, _ := s.Wait(time.Millisecond)
	if res.Val != "sota" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	res, _ = s.Wait(time.Millisecond)
	if res.Val != "caballo" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	s.Clear()
}

func TestWaitPolicy(t *testing.T) {
	ps := DefaultInstance()
	s := ps.NewSubscriber().SetQueueSize(2).SetPolicy(PolBlock)
	s.Subscribe("root")
	ps.Send(&Message{To: "root", Val: "sota"})
	ps.Send(&Message{To: "root", Val: "caballo"})
	fl := false
	go func() {
		time.Sleep(10 * time.Millisecond)
		fl = true
		res, _ := s.Wait(0)
		if res.Val != "sota" {
			t.Errorf("value missmatch %#v", res.Val)
		}
	}()
	ps.Send(&Message{To: "root", Val: "rey"})
	if !fl {
		t.Errorf("Send not block")
	}
	if s.Waiting() != 2 {
		t.Errorf("num messages missmatch")
	}

	if s.Overflow() != 0 {
		t.Errorf("Overflow in Block policy")
	}

	res, _ := s.Wait(time.Millisecond)
	if res.Val != "caballo" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	res, _ = s.Wait(time.Millisecond)
	if res.Val != "rey" {
		t.Errorf("value missmatch %#v", res.Val)
	}
	s.Clear()
}
